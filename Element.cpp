#include "Element.hpp"

Bytes Element::prime;

Element::Element
(const std::string &string) {
	integer = string;
}

Element::Element(const uint64_t &integer) {
	this->integer = integer;
}

Element::Element(const char *string){
	this->integer = string;
}

Element::Element(const Bytes &bytes) {
	this->integer = bytes;
}

Element::Element() {
	
}

void Element::print() const {
	integer.print();
}

std::string Element::string() const {
	return integer.string();
}

Element &Element::operator=(const uint64_t &integer) {
	this->integer = integer;
	return *this;
}

Element &Element::operator=(const std::string &string) {
	this->integer = string;
	return *this;
}

Element &Element::operator=(const char *string) {
	this->integer = string;
	return *this;
}

Element operator+
(const uint64_t &integer, const Element &other) {
	return Element(integer) + other;
}

Element operator+
(const std::string &string, const Element &other) {
	return Element(string) + other;
}

Element Element::operator+(const Element &other) const {
	Bytes result;
	const Bytes &a = integer;
	const Bytes &b = other.integer;
	result = prime - a;
	if(result > b) {
		result = a + b;
	}else {
		result = b - result;
	}

	return Element(result);
}

Element operator-
(const uint64_t &integer, const Element &other) {
	return Element(integer) - other;
}

Element operator-
(const std::string &string, const Element &other) {
	return Element(string) - other;
}


Element Element::operator-(const Element &other) const {
	Bytes result;
	const Bytes &a = integer;
	const Bytes &b = other.integer;
	if(a < b) {
		result = prime-b+a;
	}else {
		result = a-b;
	}
	return Element(result);
}

Element operator*
(const uint64_t &integer, const Element &other) {
	return Element(integer) * other;
}

Element operator*
(const std::string &string, const Element &other) {
	return Element(string) * other;
}

Element Element::operator*(const Element &other) const{
	Element multiply = other;
	Element result = "0";
	for(int i = 0; i < integer.bytes.size(); i++) {
		for(int j = 0; j < 8; j++) {
			if(integer.bytes[i] & (1 << j)) {
				result = result + multiply;
			}
			multiply = multiply + multiply;
		}	
	}
	return result;
}

Element operator/
(const uint64_t &integer, const Element &other) {
	return Element(integer) / other;	
}

Element operator/
(const std::string &string, const Element &other) {
	return Element(string) / other;

}

Element Element::operator/(const Element &other) const{
	
	Bytes exponent = prime - "2";
	Element result = "1";
	Element square = other;
	for(int i = 0; i < exponent.bytes.size(); i++) {
		for(int j = 0; j < 8; j++) {
			if(exponent.bytes[i] & (1 << j)) { 
				result = result * square;
			}
			square = square * square;
		}
	}
	//result.print();
	return *this * result;
}

bool operator<
(const uint64_t &integer, const Element &other) {
	return Element(integer) < other;
}

bool operator<
(const std::string &string, const Element &other) {
	return Element(string) < other;
}

bool Element::operator<(const Element &other) const {
	if(integer < other.integer) return true;
	return false;
}

bool operator>
(const uint64_t &integer, const Element &other) {
	return Element(integer) < other;
}

bool operator>
(const std::string &string, const Element &other) {
	return Element(string) < other;
}

bool Element::operator>(const Element &other) const {
	if(integer > other.integer) return true;
	return false;
}

bool operator<=
(const uint64_t &integer, const Element &other) {
	return Element(integer) <= other;
}

bool operator<=
(const std::string &string, const Element &other) {
	return Element(string) <= other;
}

bool Element::operator<=(const Element &other) const {
	if(integer <= other.integer) return true;
	return false;
}

bool operator>=
(const uint64_t &integer, const Element &other) {
	return Element(integer) >= other;
}

bool operator>=
(const std::string &string, const Element &other) {
	return Element(string) >= other;
}

bool Element::operator>=(const Element &other) const {
	if(integer >= other.integer) return true;
	return false;
}

bool operator!=
(const uint64_t &integer, const Element &other) {
	return Element(integer) != other;
}

bool operator!=
(const std::string &string, const Element &other) {
	return Element(string) != other;
}

bool Element::operator!=(const Element &other) const {
	if(integer != other.integer) return true;
	return false;
}

bool operator==
(const uint64_t &integer, const Element &other) {
	return Element(integer) == other;
}

bool operator==
(const std::string &string, const Element &other) {
	return Element(string) == other;
}

bool Element::operator==(const Element &other) const {
	if(integer == other.integer) return true;
	return false;
}
