#pragma once

#include <cstdint>
#include <fmt/core.h>

#include "Element.hpp"

class Point {
public:
Point(const std::string &x, const std::string &y);
Point(const uint64_t &x, const uint64_t &y);
Point();

void print() const;

Point operator+(const Point &other) const;
Point operator*(const Element &element) const;

bool operator==(const Point &other) const;
bool on_curve(const Point &P) const;

Element x;
Element y;

static Element a;
static Element b;
static Bytes prime;
};
