#include "Test.hpp"

void Test() {
	fmt::print("Testing\n");
	test_Bytes();
}

void error() {
	fmt::print("error\n");
	throw std::invalid_argument("test faillure");
}

void success() {
	fmt::print("success\n");
}

void test_Bytes() {
	Bytes a;
	Bytes b;

	a = "0";
	fmt::print("a == 0, ");
	a.string() == "0" ? success() : error(); 

	a = "";
	fmt::print("a == 0, ");
	a.string() == "0" ? success() : error(); 


	a = "1";
	fmt::print("a == 1, ");
	a.string() == "1" ? success() : error(); 

	a = "255";
	fmt::print("a == 255, ");
	a.string() == "255" ? success() : error(); 

	a = "256";
	fmt::print("a == 256, ");
	a.string() == "256" ? success() : error(); 

	a = "12340";
	fmt::print("a == 12340, ");
	a.string() == "12340" ? success() : error(); 

	a = "0";
	b = "0";
	fmt::print("a + b == 0, ");
	(a+b).string() == "0" ? success() : error(); 

	a = "1";
	b = "0";
	fmt::print("a + b == 1, ");
	(a+b).string() == "1" ? success() : error(); 

	a = "0";
	b = "1";
	fmt::print("a + b == 1, ");
	(a+b).string() == "1" ? success() : error(); 

	a = "1";
	b = "1";
	fmt::print("a + b == 2, ");
	(a+b).string() == "2" ? success() : error(); 

	a = "255";
	b = "255";
	fmt::print("a + b == 510, ");
	(a+b).string() == "510" ? success() : error(); 

	a = "23452355";
	b = "255";
	fmt::print("a + b == 23452610, ");
	(a+b).string() == "23452610" ? success() : error(); 

	a = "0";
	b = "0";
	fmt::print("a - b == 0, ");
	(a-b).string() == "0" ? success() : error(); 

	a = "0";
	b = "0";
	fmt::print("a - b == 0, ");
	(a-b).string() == "0" ? success() : error(); 

	a = "1";
	b = "0";
	fmt::print("a - b == 1, ");
	(a-b).string() == "1" ? success() : error(); 

	a = "1000";
	b = "0";
	fmt::print("a - b == 1000, ");
	(a-b).string() == "1000" ? success() : error(); 

	a = "1000";
	b = "1000";
	fmt::print("a - b == 0, ");
	(a-b).string() == "0" ? success() : error(); 

	a = "1245";
	b = "5";
	fmt::print("a - b == 1240, ");
	(a-b).string() == "1240" ? success() : error(); 

	a = "982736473819283756";
	b = "99233847732";
	fmt::print("a - b == 982736374585436024, ");
	(a-b).string() == "982736374585436024" ? success() : error(); 

	a = "0";
	b = "0";
	fmt::print("a * b == 0, ");
	(a*b).string() == "0" ? success() : error(); 

	a = "1";
	b = "0";
	fmt::print("a * b == 0, ");
	(a*b).string() == "0" ? success() : error(); 

	a = "3000";
	b = "0";
	fmt::print("a * b == 0, ");
	(a*b).string() == "0" ? success() : error(); 

	a = "1";
	b = "1";
	fmt::print("a * b == 1, ");
	(a*b).string() == "1" ? success() : error(); 

	a = "4320";
	b = "1";
	fmt::print("a * b == 4320, ");
	(a*b).string() == "4320" ? success() : error(); 

	a = "300";
	b = "200";
	fmt::print("a * b == 60000, ");
	(a*b).string() == "60000" ? success() : error(); 

	a = "4000";
	b = "6000";
	fmt::print("a * b == 24000000, ");
	(a*b).string() == "24000000" ? success() : error(); 

	a = "0";
	b = "0";
	fmt::print("a == b true, ");
	(a==b) == true ? success() : error(); 

	a = "1";
	b = "0";
	fmt::print("a == b false, ");
	(a==b) == false ? success() : error(); 

	a = "1";
	b = "1";
	fmt::print("a == b true, ");
	(a==b) == true ? success() : error(); 

	a = "23456664324352435";
	b = "23456664324352435";
	fmt::print("a == b true, ");
	(a==b) == true ? success() : error(); 

	a = "1213412434321";
	b = "1213412434320";
	fmt::print("a == b false, ");
	(a==b) == false ? success() : error(); 

}



