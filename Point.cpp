#include "Point.hpp"
Element Point::a;
Element Point::b;
Bytes Point::prime;

Point::Point(const std::string &x, const std::string &y) {
	this->x = x;
	this->y = y;
	Element::prime = prime;
}

Point::Point(const uint64_t &x, const uint64_t &y) {
	this->x = x;
	this->y = y;
	Element::prime = prime;
}

Point::Point() {
	Element::prime = prime;
}

void Point::print() const {
	if(!on_curve(*this)) {
		fmt::print("P({},{})\n","inf","inf");
		return;
	}
	
	fmt::print("P({},{})\n",x.string(),y.string());
}

Point Point::operator+(const Point &other) const {
	Element m;
	
	const Point &P = *this;
	const Point &Q = other;
	if(!on_curve(P)) {
		return Q;
	}else if(!on_curve(Q)) {
		return P;
	}
	Point R;

	if(P == Q) {
		m = (3*P.x*P.x+a)/(2*P.y);
	}else {
		m = (P.y-Q.y)/(P.x-Q.x);
	}

	R.x = m*m-P.x-Q.x;
	R.y = m*(P.x - R.x) - P.y;

	return R;
}

Point Point::operator*(const Element &element) const {
	Point R;
	Point P = *this;
	
	const std::vector<uint8_t> &k = 
	element.integer.bytes;
	
	for(int i = 0; i < k.size(); i++) {
		for(int j = 0; j < 8; j++) {
			if(k[i] & (1 << j)) {
				R = R + P;	
			}
			P = P + P;
		}
	}
	return R;
}

bool Point::operator==(const Point &other) const {
	if(x == other.x && y == other.y) return true;
	return false;
}

bool Point::on_curve(const Point &P) const {
	if(P.y*P.y == P.x*P.x*P.x+a*P.x+b) return true;
	return false;
}
