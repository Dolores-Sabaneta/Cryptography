#pragma once

#include <vector>
#include <cstdint>
#include <string>
#include <fmt/core.h>

class Bytes {
public:
	Bytes(const std::string &string);
	Bytes(const uint64_t &integer);
	Bytes(const char *string);
	Bytes(const std::vector<uint8_t> &bytes);
	Bytes();

	std::vector<uint8_t> bytes;
	void print() const;
	std::string string() const;

	Bytes &operator=(const uint64_t &integer);
	Bytes &operator=(const std::string &string);
	Bytes &operator=(const char *string);

	Bytes operator+(const Bytes &other) const;
	Bytes operator-(const Bytes &other) const;
	Bytes operator*(const Bytes &other) const;

	bool operator<(const Bytes &other) const;
	bool operator>(const Bytes &other) const;
	bool operator<=(const Bytes &other) const;
	bool operator>=(const Bytes &other) const;
	bool operator!=(const Bytes &other) const;
	bool operator==(const Bytes &other) const;

private:
	void print(const std::vector<uint8_t> &bytes) const;
	
	void integer_to_bytes(
		const uint64_t &integer,
		std::vector<uint8_t> &result) const;

	void string_to_bytes(
		const std::string &string,
		std::vector<uint8_t> &result) const;

	void bytes_to_string(
		const std::vector<uint8_t> &bytes, 
		std::string &string) const;

	std::vector<uint8_t> add(
		const std::vector<uint8_t> &a,
		const std::vector<uint8_t> &b) const;

	std::vector<uint8_t> multiply(
		const std::vector<uint8_t> &a,
		const std::vector<uint8_t> &b) const;
};

Bytes operator+
(const uint64_t &integer, const Bytes &other);

Bytes operator+
(const std::string &string, const Bytes &other);

Bytes operator-
(const uint64_t &integer, const Bytes &other);

Bytes operator-
(const std::string &string, const Bytes &other);

Bytes operator*
(const uint64_t &integer, const Bytes &other);

Bytes operator*
(const std::string &string, const Bytes &other);

bool operator<
(const uint64_t &integer, const Bytes &other);

bool operator<
(const std::string &string, const Bytes &other);

bool operator>
(const uint64_t &integer, const Bytes &other);

bool operator>
(const std::string &string, const Bytes &other);

bool operator<=
(const uint64_t &integer, const Bytes &other);

bool operator<=
(const std::string &string, const Bytes &other);

bool operator>=
(const uint64_t &integer, const Bytes &other);

bool operator>=
(const std::string &string, const Bytes &other);

bool operator!=
(const uint64_t &integer, const Bytes &other);

bool operator!=
(const std::string &string, const Bytes &other);

bool operator==
(const uint64_t &integer, const Bytes &other);

bool operator==
(const std::string &string, const Bytes &other);
