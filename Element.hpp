#pragma once

#include <vector>
#include <cstdint>
#include <string>
#include <fmt/core.h>

#include "Bytes.hpp"

class Element {
public:
	Element(const std::string &string);
	Element(const uint64_t &integer);
	Element(const char *string);
	Element(const Bytes &bytes);
	Element();
	void print() const;
	std::string string() const;
	
	Element &operator=(const uint64_t &integer);
	Element &operator=(const std::string &string);
	Element &operator=(const char *string);

	Element operator+(const Element &other) const;
	Element operator-(const Element &other) const;
	Element operator*(const Element &other) const;
	Element operator/(const Element &other) const;

	bool operator<(const Element &other) const;
	bool operator>(const Element &other) const;
	bool operator<=(const Element &other) const;
	bool operator>=(const Element &other) const;
	bool operator!=(const Element &other) const;
	bool operator==(const Element &other) const;

	Bytes integer;
	static Bytes prime;
};

Element operator+
(const uint64_t &integer, const Element &other);

Element operator+
(const std::string &string, const Element &other);

Element operator-
(const uint64_t &integer, const Element &other);

Element operator-
(const std::string &string, const Element &other);

Element operator*
(const uint64_t &integer, const Element &other);

Element operator*
(const std::string &string, const Element &other);

Element operator/
(const uint64_t &integer, const Element &other);

Element operator/
(const std::string &string, const Element &other);

bool operator<
(const uint64_t &integer, const Element &other);

bool operator<
(const std::string &string, const Element &other);

bool operator>
(const uint64_t &integer, const Element &other);

bool operator>
(const std::string &string, const Element &other);

bool operator<=
(const uint64_t &integer, const Element &other);

bool operator<=
(const std::string &string, const Element &other);

bool operator>=
(const uint64_t &integer, const Element &other);

bool operator>=
(const std::string &string, const Element &other);

bool operator!=
(const uint64_t &integer, const Element &other);

bool operator!=
(const std::string &string, const Element &other);

bool operator==
(const uint64_t &integer, const Element &other);

bool operator==
(const std::string &string, const Element &other);
