#include "Bytes.hpp"

Bytes::Bytes(const std::string &string) {
	string_to_bytes(string,bytes);
}

Bytes::Bytes(const std::vector<uint8_t> &bytes) {
	this->bytes = bytes;
}

Bytes::Bytes(const uint64_t &integer) {
	integer_to_bytes(integer,bytes);
}

Bytes::Bytes(const char *string) {
	string_to_bytes(string,bytes);
}

Bytes::Bytes() {

}

void Bytes::integer_to_bytes(
const uint64_t &integer,
std::vector<uint8_t> &result) const {

	result.clear();
	uint64_t value = integer;
	while(value > 0) {
		uint8_t byte = value;
		result.push_back(byte);
		value = value >> 8;
	}
}

void Bytes::string_to_bytes(
const std::string &string, 
std::vector<uint8_t> &result) const {

	result.clear();
	std::vector<uint8_t> dividend;
	//fmt::print("string:{}\n",string);
	for(int i = 0; i < string.size(); i++) {
		if(string[i] - '0' < 0 || string[i] - '0' > 9)
			throw std::invalid_argument("not an int");

		dividend.push_back(string[i] - '0');
	}
	for(int i = 0; i < dividend.size(); i++) {
		if(dividend[i] == 0) {
			dividend.erase(dividend.begin());
		}else {
			break;
		}
	}
	
	while(dividend.size() > 0) {
		bool start = false;
		int remainder = 0;
		std::vector<uint8_t> quotient;
		for(int i = 0; i < dividend.size(); i++) {
			remainder = remainder * 10 + dividend[i];
			if(remainder > 255)start = true;
			if(start) quotient.push_back(remainder/256);
			remainder = remainder % 256;
		}
		result.push_back(remainder);
		dividend = quotient;
	}
}

void Bytes::bytes_to_string(
const std::vector<uint8_t> &bytes, 
std::string &string) const{

	string.clear();
	std::vector<uint8_t> result;
	std::vector<uint8_t> base = {1};
	for(int i = 0; i < bytes.size(); i++) {
		for(int j = 0; j < 8; j++) {
			if(bytes[i] & 1 << j) {
				result = add(result, base);
			}
			base = add(base,base);
		}
	}
	//print(result);
	if(result.size() == 0) {
		string = "0";
		return;
	}
	for(int i = result.size()-1; i >= 0; i--) {
		char s = result[i]+'0';
		string.append(1,s);
	}
}

std::vector<uint8_t> Bytes::add(
const std::vector<uint8_t> &a,
const std::vector<uint8_t> &b) const{

	std::vector<uint8_t> result;
	bool carry = 0;
	int i = 0;
	int sa = a.size();
	int sb = b.size();
	while(i < sa || i < sb || carry == 1) {
		int sum = 0;
		if(i < sa) {
			if(a[i] > 10) 
			throw std::invalid_argument("not base 10");
			sum = sum + a[i];
		}
		if(i < sb) {
			if(b[i] > 10) 
			throw std::invalid_argument("not base 10");
			sum = sum + b[i];
		}
		sum = sum + carry;
		carry = sum < 10 ? 0 : 1;
		sum = sum % 10;
		result.push_back(sum);
		i++;
	}
	//print(result);
	return result;
}

std::vector<uint8_t> Bytes::multiply(
const std::vector<uint8_t> &a,
const std::vector<uint8_t> &b) const {
	
	std::vector<uint8_t> result;
	int factor = 0;
	int carry = 0;

	for(int i = 0; i < a.size(); i++) {
		std::vector<uint8_t> line;
		for(int k = 0; k < i; k++) line.push_back(0);
		for(int j = 0; j < b.size() || carry > 0; j++) {
			if(j == b.size()) {
				factor = carry;
			}else {
				factor = a[i] * b[j] + carry;
			}
			carry = factor / 10;
			factor = factor % 10;
			//fmt::print("factor:{},carry:{}\n",factor,carry);
			line.push_back(factor);
		}
		result = add(result,line);
	}
	//print(result);
	return result;
}

void Bytes::print() const {
	std::string string;
	bytes_to_string(bytes,string);
	fmt::print("{}\n",string);
}

void Bytes::print(
const std::vector<uint8_t> &bytes) const {

	for(int i = bytes.size() - 1; i >= 0; i--) {
		fmt::print("{}.",bytes[i]);
	}
	fmt::print("\n");
}

std::string Bytes::string() const {
	std::string string;
	bytes_to_string(bytes,string);
	return string;
}

Bytes &Bytes::operator=(const uint64_t &integer) {
	integer_to_bytes(integer,bytes);
	return *this;
}

Bytes &Bytes::operator=(const std::string &string) {
	string_to_bytes(string,bytes);
	return *this;
}

Bytes &Bytes::operator=(const char *string) {
	string_to_bytes(string,bytes);
	return *this;
}

Bytes operator+
(const uint64_t &integer, const Bytes &other) {
	return Bytes(integer) + other;
}

Bytes operator+
(const std::string &string, const Bytes &other) {
	return Bytes(string) + other;
}

Bytes Bytes::operator+(const Bytes &other) const{
	std::vector<uint8_t> result;

	int b = bytes.size();
	int o = other.bytes.size();
	bool carry = 0;
	int i = 0;

	while(i < b || i < o || carry == 1) {
		int sum = 0;
		if(i < b) {
			sum = sum + bytes[i];
		}
		if(i < o) {
			sum = sum + other.bytes[i];
		}
		sum = sum + carry;
		carry = sum < 256 ?  0 : 1;
		sum = sum % 256;
		result.push_back(sum);
		i++;
	}

	//print(result);
	return Bytes(result);
}

Bytes operator-
(const uint64_t &integer, const Bytes &other) {
	return Bytes(integer) - other;
}

Bytes operator-
(const std::string &string, const Bytes &other) {
	return Bytes(string) - other;
}

Bytes Bytes::operator-(const Bytes &other) const{
	if(*this < other) { 
		throw std::invalid_argument
		("operator- gives negative result");
	}

	std::vector<uint8_t> result;
	const std::vector<uint8_t> &a = bytes;
	const std::vector<uint8_t> &b = other.bytes;
	
	bool carry = 1;
	for(int i = 0; i < a.size(); i++) {
		uint8_t complement = i < b.size() ? ~b[i] : 255;
		uint8_t value = a[i] + complement + carry;
		carry = value - carry < complement ? 1 : 0;
		result.push_back(value);
	}

	return Bytes(result);
}

Bytes operator*
(const uint64_t &integer, const Bytes &other) {
	return Bytes(integer) * other;
}

Bytes operator*
(const std::string &string, const Bytes &other) {
	return Bytes(string) * other;
}

Bytes Bytes::operator*(const Bytes &other) const {
	Bytes result;
	Bytes d = other;

	for(int i = 0; i < bytes.size(); i++) {
		for(int j = 0; j < 8; j++) {
			if(bytes[i] & 1 << j) {
				result = result + d;
			}
			d = d+d;
		}
	}
	return Bytes(result);
}

bool operator<
(const uint64_t &integer, const Bytes &other) {
	return Bytes(integer) < other;
}

bool operator<
(const std::string &string, const Bytes &other) {
	return Bytes(string) < other;
}

bool Bytes::operator<(const Bytes &other) const{
	bool result = false;	
	if(bytes.size() < other.bytes.size()) {
		result = true;
	}else if(bytes.size() == other.bytes.size()) {
		for(int i = bytes.size()-1; i >= 0; i--) {
			if(bytes[i] < other.bytes[i]) {
				result = true;
				break;
			}else if(bytes[i] > other.bytes[i]) {
				result = false;
				break;
			}
		}
	}
	return result;
}

bool operator>
(const uint64_t &integer, const Bytes &other) {
	return Bytes(integer) > other;
}

bool operator>
(const std::string &string, const Bytes &other) {
	return Bytes(string) > other;
}

bool Bytes::operator>(const Bytes &other) const{
	bool result = false;	
	if(bytes.size() > other.bytes.size()) {
		result = true;
	}else if(bytes.size() == other.bytes.size()) {
		for(int i = bytes.size()-1; i >= 0; i--) {
			if(bytes[i] > other.bytes[i]) {
				result = true;
				break;
			}else if(bytes[i] < other.bytes[i]) {
				result = false;
				break;
			}
		}
	}
	return result;
}

bool operator<=
(const uint64_t &integer, const Bytes &other) {
	return Bytes(integer) <= other;
}

bool operator<=
(const std::string &string, const Bytes &other) {
	return Bytes(string) <= other;
}

bool Bytes::operator<=(const Bytes &other) const {
	if(bytes.size() < other.bytes.size()) {
		return true;
	}else if(bytes.size() == other.bytes.size()) {
		for(int i = bytes.size()-1; i >= 0; i--) {
			if(bytes[i] < other.bytes[i]) {
				return true;
			}else if(bytes[i] > other.bytes[i]) {
				return false;
			}
		}
		return true;
	}
	return false;
}

bool operator>=
(const uint64_t &integer, const Bytes &other) {
	return Bytes(integer) >= other;
}

bool operator>=
(const std::string &string, const Bytes &other) {
	return Bytes(string) >= other;
}

bool Bytes::operator>=(const Bytes &other) const {
	if(bytes.size() > other.bytes.size()) {
		return true;
	}else if(bytes.size() == other.bytes.size()) {
		for(int i = bytes.size()-1; i >= 0; i--) {
			if(bytes[i] > other.bytes[i]) {
				return true;
			}else if(bytes[i] < other.bytes[i]) {
				return false;
			}
		}
		return true;
	}
	return false;
}

bool operator!=
(const uint64_t &integer, const Bytes &other) {
	return Bytes(integer) != other;
}

bool operator!=
(const std::string &string, const Bytes &other) {
	return Bytes(string) != other;
}

bool Bytes::operator!=(const Bytes &other) const {
	if(bytes.size() == other.bytes.size()) {
		for(int i = 0; i < bytes.size(); i++) {
			if(bytes[i] != other.bytes[i]) return true;
		}
		return false;
	}
	return true;
}

bool operator==
(const uint64_t &integer, const Bytes &other) {
	return Bytes(integer) == other;
}

bool operator==
(const std::string &string, const Bytes &other) {
	return Bytes(string) == other;
}

bool Bytes::operator==(const Bytes &other) const {
	if(bytes.size() == other.bytes.size()) {
		for(int i = 0; i < bytes.size(); i++) {
			if(bytes[i] != other.bytes[i]) return false;
		}
		return true;
	}
	return false;
}
